# Migrate all VM disks on the node to a different storage
import argparse
from drewsTools.l0 import proxmox
from drewsTools.l1 import proxmoxFunctions
import logging
import sys
root = logging.getLogger()
root.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)


def getArgs():
    parser = argparse.ArgumentParser(description='Move disks elsewhere')
    parser.add_argument(
        'nodeName',
        metavar='nodeName',
        type=str,
        help='The name of the node we work on.'
    )
    parser.add_argument(
        'storageName',
        metavar='storageName',
        type=str,
        help='The name of storage to migrate disks to.'
    )
    return parser.parse_args()

def main():
    args = getArgs()
    print(args.nodeName)
    Proxmox=proxmox.Proxmox()


    # cycle through VMs on this node
    for vm in Proxmox.getVms():
        if vm['node'] != args.nodeName:
            continue
        proxmoxFunctions.migrateVmDisksToStorage(vm, args.storageName)

if __name__ == "__main__":
    main()